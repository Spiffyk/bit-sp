pub const U64_WORDS: usize = std::mem::size_of::<u64>() / 2;
pub const U64_BYTES: usize = std::mem::size_of::<u64>();
pub const U128_WORDS: usize = std::mem::size_of::<u128>() / 2;
pub const U128_BYTES: usize = std::mem::size_of::<u128>();

/// A trait allowing to manipulate a piece of data with u16.
pub trait WordSplit {
    unsafe fn get_word(&self, i: usize) -> u16;
    unsafe fn set_word(&mut self, i: usize, word: u16);

    /// Converts a slice of words into Self.
    fn from_words(words: &[u16]) -> Self;

    /// Converts self into a slice of words.
    fn to_words(&self) -> Box<[u16]>;
}

pub trait ByteSplit {
    unsafe fn get_byte(&self, i: usize) -> u8;
    unsafe fn set_byte(&mut self, i: usize, byte: u8);

    /// Converts a slice of bytes into Self.
    fn from_bytes(bytes: &[u8]) -> Self;

    fn to_bytes(&self) -> Box<[u8]>;
}


union U64Split {
    value: u64,
    words: [u16; 4],
    bytes: [u8; 8]
}

impl WordSplit for u64 {
    unsafe fn get_word(&self, i: usize) -> u16 {
        return U64Split { value: self.to_le() }.words[U64_WORDS - 1 - i];
    }

    unsafe fn set_word(&mut self, i: usize, word: u16) {
        let mut split = U64Split { value: self.to_le() };
        split.words[U64_WORDS - 1 - i] = word;
        *self = u64::from_le(split.value);
    }

    fn from_words(words: &[u16]) -> u64 {
        unsafe {
            assert_eq!(words.len(), U64_WORDS);
            let mut result = 0u64;
            for i in 0..U64_WORDS {
                result.set_word(i, words[i]);
            }
            return result;
        }
    }

    fn to_words(&self) -> Box<[u16]> {
        unsafe {
            let mut words = U64Split { value: self.to_le() }.words;
            words.reverse();
            return words.to_vec().into_boxed_slice();
        }
    }
}

impl ByteSplit for u64 {
    unsafe fn get_byte(&self, i: usize) -> u8 {
        return U64Split { value: self.to_le() }.bytes[U64_BYTES - 1 - i];
    }

    unsafe fn set_byte(&mut self, i: usize, byte: u8) {
        let mut split = U64Split { value: self.to_le() };
        split.bytes[U64_BYTES - 1 - i] = byte;
        *self = u64::from_le(split.value);
    }

    fn from_bytes(bytes: &[u8]) -> u64 {
        unsafe {
            assert_eq!(bytes.len(), U64_BYTES);
            let mut result = 0u64;
            for i in 0..U64_BYTES {
                result.set_byte(i, bytes[i]);
            }
            return result;
        }
    }

    fn to_bytes(&self) -> Box<[u8]> {
        unsafe {
            let mut bytes = U64Split { value: self.to_le() }.bytes;
            bytes.reverse();
            return bytes.to_vec().into_boxed_slice();
        }
    }
}


union U128Split {
    value: u128,
    words: [u16; U128_WORDS],
    bytes: [u8; U128_BYTES]
}

impl WordSplit for u128 {
    unsafe fn get_word(&self, i: usize) -> u16 {
        return U128Split { value: self.to_le() }.words[U128_WORDS - 1 - i];
    }

    unsafe fn set_word(&mut self, i: usize, word: u16) {
        let mut split = U128Split { value: self.to_le() };
        split.words[U128_WORDS - 1 - i] = word;
        *self = u128::from_le(split.value);
    }

    fn from_words(words: &[u16]) -> u128 {
        unsafe {
            assert_eq!(words.len(), U128_WORDS);
            let mut result = 0u128;
            for i in 0..U128_WORDS {
                result.set_word(i, words[i]);
            }
            return result;
        }
    }

    fn to_words(&self) -> Box<[u16]> {
        unsafe {
            let mut words = U128Split { value: self.to_le() }.words;
            words.reverse();
            return words.to_vec().into_boxed_slice();
        }
    }
}

impl ByteSplit for u128 {
    unsafe fn get_byte(&self, i: usize) -> u8 {
        return U128Split { value: self.to_le() }.bytes[U128_BYTES - 1 - i];
    }

    unsafe fn set_byte(&mut self, i: usize, byte: u8) {
        let mut split = U128Split { value: self.to_le() };
        split.bytes[U128_BYTES - 1 - i] = byte;
        *self = u128::from_le(split.value);
    }

    fn from_bytes(bytes: &[u8]) -> u128 {
        unsafe {
            assert_eq!(bytes.len(), U128_BYTES);
            let mut result = 0u128;
            for i in 0..U128_BYTES {
                result.set_byte(i, bytes[i]);
            }
            return result;
        }
    }

    fn to_bytes(&self) -> Box<[u8]> {
        unsafe {
            let mut bytes = U128Split { value: self.to_le() }.bytes;
            bytes.reverse();
            return bytes.to_vec().into_boxed_slice();
        }
    }
}
