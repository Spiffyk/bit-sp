extern crate modinverse;
pub mod numsplits;

use modinverse::modinverse;
use numsplits::WordSplit;


struct KeyScheduler {
    original_key: u128
}

/// A key scheduler for the International Data Encryption Algorithm (a.k.a. IDEA).
impl KeyScheduler {

    /// Creates a new key scheduler with the specified `key`.
    pub fn new(key: u128) -> KeyScheduler {
        return KeyScheduler {
            original_key: key
        };
    }

    /// Gets an encryption subkey on the specified position `k`.
    pub fn get(&self, k: usize) -> u16 {
        unsafe {
            let index = k % 8;
            let rotations = (k / 8) as u32;
            return self.original_key.rotate_left(25 * rotations).get_word(index);
        }
    }

    /// Gets a decryption subkey on the specified position `k`.
    pub fn getd(&self, k: usize) -> u16 {
        let offset = 52 * (k / 52);
        let k_mod = k % 52;
        if k_mod < 4 {
            match k_mod {
                0 => return mul_inv(self.get(offset + 48)),
                1 => return add_inv(self.get(offset + 49)),
                2 => return add_inv(self.get(offset + 50)),
                3 => return mul_inv(self.get(offset + 51)),

                _ => unimplemented!("It is unbelievable what happened here.")
            }
        } else if k_mod >= 48 {
            match k_mod {
                48 => return mul_inv(self.get(offset + 0)),
                49 => return add_inv(self.get(offset + 1)),
                50 => return add_inv(self.get(offset + 2)),
                51 => return mul_inv(self.get(offset + 3)),

                _ => unimplemented!("It is unbelievable what happened here.")
            }
        } else {
            let m = (k_mod - 4) % 6;
            let iteration = (k_mod - 4) / 6;
            let base = offset + 47 - iteration * 6;

            match m {
                0 => return self.get(base - 1),
                1 => return self.get(base),

                2 => return mul_inv(self.get(base - 5)),
                3 => return add_inv(self.get(base - 3)),
                4 => return add_inv(self.get(base - 4)),
                5 => return mul_inv(self.get(base - 2)),

                _ => unimplemented!("It is unbelievable what happened here.")
            }
        }
    }
}

pub fn encrypt_block(key: u128, block_offset: usize, data: u64) -> u64 {
    unsafe {
        let k = KeyScheduler::new(key);
        let key_offset = 52 * block_offset;
        let mut data = data;

        for n in 0..8 {
            let o = key_offset + 6 * n;
            data = process_round(
                [
                    k.get(o),
                    k.get(o + 1),
                    k.get(o + 2),
                    k.get(o + 3),
                    k.get(o + 4),
                    k.get(o + 5)
                ], data);
        }
        data = process_transform([
                                     k.get(key_offset + 48),
                                     k.get(key_offset + 49),
                                     k.get(key_offset + 50),
                                     k.get(key_offset + 51)
                                 ], data);

        return data;
    }
}

pub fn decrypt_block(key: u128, block_offset: usize, data: u64) -> u64 {
    unsafe {
        let k = KeyScheduler::new(key);
        let key_offset = 52 * block_offset;
        let mut data = data;

        for n in 0..8 {
            let o = key_offset + 6 * n;
            data = process_round(
                [
                    k.getd(o),
                    k.getd(o + 1),
                    k.getd(o + 2),
                    k.getd(o + 3),
                    k.getd(o + 4),
                    k.getd(o + 5)
                ], data);
        }
        data = process_transform(
            [
                k.getd(key_offset + 48),
                k.getd(key_offset + 49),
                k.getd(key_offset + 50),
                k.getd(key_offset + 51)
            ], data);

        return data;
    }
}

unsafe fn process_round(key: [u16; 6], data: u64) -> u64 {
    let a0 = mul_mod(key[0], data.get_word(0));
    let a1 = add_mod(key[1], data.get_word(1));
    let a2 = add_mod(key[2], data.get_word(2));
    let a3 = mul_mod(key[3], data.get_word(3));

    let b0 = a0 ^ a2;
    let b1 = a1 ^ a3;

    let c0 = mul_mod(key[4], b0);
    let c1 = add_mod(c0, b1);

    let d0 = mul_mod(key[5], c1);
    let d1 = add_mod(c0, d0);

    let e0 = a0 ^ d0;
    let e1 = a1 ^ d1;
    let e2 = a2 ^ d0;
    let e3 = a3 ^ d1;


    let mut result = 0u64;
    result.set_word(0, e0);
    result.set_word(1, e2); // e2 and e1 are swapped on purpose
    result.set_word(2, e1); // e2 and e1 are swapped on purpose
    result.set_word(3, e3);
    return result;
}

unsafe fn process_transform(key: [u16; 4], data: u64) -> u64 {
    let mut result = 0u64;
    result.set_word(0, mul_mod(key[0], data.get_word(0)));
    result.set_word(1, add_mod(key[1], data.get_word(2))); // words swapped on purpose
    result.set_word(2, add_mod(key[2], data.get_word(1))); // words swapped on purpose
    result.set_word(3, mul_mod(key[3], data.get_word(3)));
    return result;
}

fn add_mod(a: u16, b: u16) -> u16 {
    return a.wrapping_add(b);
}

fn add_inv(a: u16) -> u16 {
    return a.wrapping_neg();
}

fn mul_mod(a: u16, b: u16) -> u16 {
    let a: u64 = if a == 0 { 0x10000 } else { a as u64 };
    let b: u64 = if b == 0 { 0x10000 } else { b as u64 };
    let result = (a * b) % 0x10001;
    return if result == 0x10000 {
        0
    } else {
        result as u16
    };
}

fn mul_inv(a: u16) -> u16 {
    let a: i64 = if a == 0 { 0x10000 } else { a as i64 };
    let result = modinverse(a, 0x10001).expect("Can't get multiplicative inverse!");
    return if result == 0x10000 {
        0
    } else {
        result as u16
    };
}
