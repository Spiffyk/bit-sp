extern crate atty;
extern crate clap;

use atty::Stream;
use clap::{App, Arg, SubCommand};
use std::io;
use std::io::{Read, Write};
use std::str::FromStr;
use idea::numsplits::{ByteSplit, U64_BYTES};

/// Calculates an unsigned 128-bit hash code of the specified string, using
/// Dan Bernstein's [djb2 hash function](http://www.cse.yorku.ca/~oz/hash.html).
fn string_to_key(string: &str) -> u128 {
    return u128::from_str(string)
        .unwrap_or_else(|_| {
            eprintln!("WARNING: The key does not represent an unsigned 128-bit number. Using \
            a djb2 hash function to generate one.");

            let bytes = string.as_bytes();
            let mut hash = 5381 as u128;
            bytes.iter().for_each(|byte| {
                hash = hash.wrapping_shl(25).wrapping_add(hash).wrapping_add((*byte) as u128);
            });
            return hash;
        });
}

fn encrypt(key: u128) {
    let mut block_offset = 0usize;
    loop {
        let mut buffer = [0u8; U64_BYTES];
        let bytes_read = io::stdin().read(&mut buffer)
            .expect("Read error!");

        if bytes_read == 0 {
            // Complete data block (no padding).
            // Writing extra block filled with zeroes.
            let out_data = idea::encrypt_block(key, block_offset, 0);
            io::stdout().write_all(&out_data.to_bytes())
                .expect("Write error!");
            return;
        }

        // Encrypt data.
        let in_data = u64::from_bytes(&buffer);
        let out_data = idea::encrypt_block(key, block_offset, in_data);
        block_offset += 1;
        io::stdout().write_all(&out_data.to_bytes())
            .expect("Write error!");

        if bytes_read < U64_BYTES {
            // Incomplete data block (padded by zeroes).
            // Writing extra block with the number of bytes to strip during decryption.
            let strip_bytes = U64_BYTES - bytes_read;
            let out_data = idea::encrypt_block(key, block_offset, strip_bytes as u64);
            io::stdout().write_all(&out_data.to_bytes())
                .expect("Write error!");
            return;
        }
    }
}

fn decrypt(key: u128) {
    let mut block_offset = 0usize;
    let mut decrypted_data: u64;
    let mut pre_out_data: u64;
    let mut out_data: u64;

    let mut buffer = [0u8; U64_BYTES];
    let bytes_read = io::stdin().read(&mut buffer)
        .expect("Read error!");

    if bytes_read < U64_BYTES {
        eprintln!("WARNING: The input ended unexpectedly!");
        return;
    }

    let in_data = u64::from_bytes(&buffer);
    decrypted_data = idea::decrypt_block(key, block_offset, in_data);
    block_offset += 1;

    let mut buffer = [0u8; U64_BYTES];
    let bytes_read = io::stdin().read(&mut buffer)
        .expect("Read error!");

    if bytes_read < U64_BYTES {
        eprintln!("WARNING: The input ended unexpectedly!");
        return;
    }

    pre_out_data = decrypted_data;
    let in_data = u64::from_bytes(&buffer);
    decrypted_data = idea::decrypt_block(key, block_offset, in_data);
    block_offset += 1;

    loop {
        let mut buffer = [0u8; U64_BYTES];
        let bytes_read = io::stdin().read(&mut buffer)
            .expect("Read error!");

        if bytes_read == 0 {
            let stripped_bytes = decrypted_data as usize;
            out_data = pre_out_data;
            io::stdout().write_all(&(out_data.to_bytes()[..U64_BYTES - stripped_bytes]))
                .expect("Write error!");
            return;
        }
        if bytes_read < U64_BYTES {
            eprintln!("WARNING: The input ended unexpectedly!");
            return;
        }

        out_data = pre_out_data;
        pre_out_data = decrypted_data;
        let in_data = u64::from_bytes(&buffer);
        decrypted_data = idea::decrypt_block(key, block_offset, in_data);
        block_offset += 1;
        io::stdout().write_all(&out_data.to_bytes())
            .expect("Write error!");
    }
}

/// Program entry point.
fn main() {
    let args = App::new("Rusty IDEA")
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .version_short("v")
        .subcommand(SubCommand::with_name("encrypt")
            .alias("e")
            .about("Reads input from stdin, encrypts it and writes it into stdout.")
            .arg(Arg::with_name("key")
                .short("k")
                .long("key")
                .required(true)
                .value_name("KEY")
                .help("The key for encryption")))
        .subcommand(SubCommand::with_name("decrypt")
            .alias("d")
            .about("Reads input from stdin, encrypts it and writes it into stdout.")
            .arg(Arg::with_name("key")
                .short("k")
                .long("key")
                .required(true)
                .value_name("KEY")
                .help("The key for decryption")))
        .get_matches();

    if atty::is(Stream::Stdin) || atty::is(Stream::Stdout) {
        eprintln!("WARNING: Using this tool without redirecting both stdin and stdout is \
        discouraged!");
    }

    if let Some(subargs) = args.subcommand_matches("encrypt") {
        let key = string_to_key(
            subargs.value_of("key").expect("Missing key!"));
        encrypt(key);
    } else if let Some(subargs) = args.subcommand_matches("decrypt") {
        let key = string_to_key(
            subargs.value_of("key").expect("Missing key!"));
        decrypt(key);
    } else {
        eprintln!("Missing subcommand! Use --help to list subcommands.");
    }
}


